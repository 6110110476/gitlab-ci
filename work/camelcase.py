'''
    This module is camelcase generator.
'''


def camelcase(words):
    '''
        Function will return word_count
    '''

    word_count = 1
    for i in enumerate(words):
        if words[i].isupper() == 1:
            word_count += 1
    return word_count


if __name__ == '__main__':
    word = input()
    RESULT = camelcase(word)
    print(RESULT)
