'''
    This module is test_camelcase generator.
'''

import unittest
import work.camelcase as camelcase


class TestCamelcase(unittest.TestCase):
    '''
            Class will check cases
    '''

    def test_give_one_word_should_be_1(self):
        '''
            Function will test assertEqual
        '''
        self.assertEqual(camelcase.camelcase("word"), 1, 'Should be 1')

    def test_give_two_word_should_be_2(self):
        '''
            Function will test assertEqual
        '''
        self.assertEqual(camelcase.camelcase("wordWord"), 2, 'Should be 2')

    def test_give_three_word_should_be_3(self):
        '''
            Function will test assertEqual
        '''
        self.assertEqual(camelcase.camelcase("wordWordWord"), 3, 'Should be 3')


if __name__ == '__main__':
    unittest.main()
